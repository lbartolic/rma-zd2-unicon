package com.example.asus.zadaca2;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ConversionActivity extends AppCompatActivity {
    private String[] arraySpinner;
    private RelativeLayout convLayout;
    private TextView convTitle;
    private Button conversionBtn;
    private Spinner convSpinnerFrom;
    private Spinner convSpinnerTo;

    public ConversionActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversion);

        this.convLayout = (RelativeLayout) findViewById(R.id.conversionLayout);
        this.convTitle = (TextView) findViewById(R.id.convTitle);
        this.conversionBtn = (Button) findViewById(R.id.conversionBtn);
        this.convSpinnerFrom = (Spinner) findViewById(R.id.convSpinnerFrom);
        this.convSpinnerTo = (Spinner) findViewById(R.id.convSpinnerTo);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras.containsKey("msg")){
            String msg = extras.getString("msg");
            handleConversionLayout(msg);
        }
        else {
            finish();
        }
    }

    private void handleConversionLayout(String msg) {
        this.arraySpinner = new ConversionHelper().getConversionData(msg);

        Spinner sFrom = (Spinner) findViewById(R.id.convSpinnerFrom);
        Spinner sTo = (Spinner) findViewById(R.id.convSpinnerTo);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.spinner, arraySpinner);

        sFrom.setAdapter(adapter);
        sTo.setAdapter(adapter);

        final String conversionType = msg;

        conversionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleConversion(conversionType);
            }
        });

        String bcg = null;
        String text = null;

        switch (conversionType) {
            case "temperature":
                bcg = "#7c2020";
                text = "Temperature conversion";
                break;
            case "distance":
                bcg = "#206c76";
                text = "Distance conversion";
                break;
            case "volume":
                bcg = "#4f2280";
                text = "Volume conversion";
                break;
            case "speed":
                bcg = "#25872a";
                text = "Currency conversion";
                break;
        }

        convLayout.setBackgroundColor(Color.parseColor(bcg));
        convTitle.setText(text);
    }

    private void handleConversion(String convType) {
        ConversionHelper conversionHelper = new ConversionHelper();

        String from = convSpinnerFrom.getSelectedItem().toString();
        String to = convSpinnerTo.getSelectedItem().toString();

        EditText convFromInput = (EditText) findViewById(R.id.convFromInput);

        double value = 0;
        try {

            value = Double.parseDouble(convFromInput.getText().toString());

            double result = 0;

            switch (convType) {
                case "temperature":
                    result = conversionHelper.tempConvert(value, from, to);
                    break;
                case "distance":
                    result = conversionHelper.lengthConvert(value, from, to);
                    break;
                case "volume":
                    result = conversionHelper.volumeConvert(value, from, to);
                    break;
                case "speed":
                    result = conversionHelper.speedConvert(value, from, to);
            }

            Log.i("RESULT", result + "");

            Intent i = new Intent();
            i.putExtra("from", from);
            i.putExtra("to", to);
            i.putExtra("value", value);
            i.putExtra("result", result);
            i.setClass(this, ResultActivity.class);
            startActivity(i);
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }

    }
}
