package com.example.asus.zadaca2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        String from = extras.getString("from");
        String to = extras.getString("to");
        double value = extras.getDouble("value");
        double result = extras.getDouble("result");

        TextView fromValue = (TextView) findViewById(R.id.fromValue);
        TextView toValue = (TextView) findViewById(R.id.toValue);
        TextView unitFrom = (TextView) findViewById(R.id.unitFrom);
        TextView unitTo = (TextView) findViewById(R.id.unitTo);


        String valueRounded = String.format("%.2f", value);
        String resultRounded = String.format("%.2f", result);

        if (fromValue != null) {
            fromValue.setText(valueRounded);
        }
        if (toValue != null) {
            toValue.setText(resultRounded);
        }
        if (unitFrom != null) {
            unitFrom.setText(from);
        }
        if (unitTo != null) {
            unitTo.setText(to);
        }

    }
}
