package com.example.asus.zadaca2;

import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout llTemperature, llDistance, llVolume, llCurrency;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        llTemperature = (LinearLayout) findViewById(R.id.temp);
        llDistance = (LinearLayout) findViewById(R.id.dist);
        llVolume = (LinearLayout) findViewById(R.id.vol);
        llCurrency = (LinearLayout) findViewById(R.id.speed);

        llTemperature.setOnClickListener(this);
        llDistance.setOnClickListener(this);
        llVolume.setOnClickListener(this);
        llCurrency.setOnClickListener(this);

        initialize();
    }

    protected void initialize() {

    }

    public void onClick(View v) {
        Intent i = new Intent();
        String cond = null;
        String bcg = null;
        switch (v.getId()){
            case R.id.temp:
                cond = "temperature";
                break;
            case R.id.dist:
                cond = "distance";
                break;
            case R.id.vol:
                cond = "volume";
                break;
            case R.id.speed:
                cond = "speed";
                break;
        }
        i.putExtra("msg", cond);
        i.putExtra("bcg", bcg);
        i.setClass(this, ConversionActivity.class);
        startActivity(i);
    }
}
